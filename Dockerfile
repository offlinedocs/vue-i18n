# Build
FROM node:latest AS builder

RUN git clone https://github.com/kazupon/vue-i18n.git /vue-i18n

WORKDIR /vue-i18n

RUN npm install
RUN npm run docs:build

# Serve
FROM nginx:alpine

COPY --from=builder /vue-i18n/docs /usr/share/nginx/html
